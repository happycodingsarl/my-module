<?php

namespace Drupal\my\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class HappyCodingController.
 */
class HappyCodingController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The my.settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a new HappyCodingController.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   */
  public function __construct(
    AccountInterface $current_user,
    ConfigFactory $config_factory
  ) {
    $this->currentUser = $current_user;
    $this->config = $config_factory->get('my.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * Redirect to happy coding project page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   A TrustedRedirectResponse object.
   */
  public function productPage(Request $request) {
    if (!($base_url = $this->config->get('api_base_url'))) {
      throw new AccessDeniedHttpException('Missed api base url.');
    }
    $url = Url::fromUri($base_url);
    $url->setOption('query', [
      'site' => $request->getHost(),
      'email' => $this->currentUser->getEmail(),
    ]);

    return new TrustedRedirectResponse($url->toString());
  }

}
