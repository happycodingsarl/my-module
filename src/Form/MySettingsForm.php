<?php

namespace Drupal\my\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure My settings.
 */
class MySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'my_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['my.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_base_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Api base url'),
      '#default_value' => $this->config('my.settings')->get('api_base_url'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('my.settings')
      ->set('api_base_url', $form_state->getValue('api_base_url'))
      ->save();

    parent::submitForm($form, $form_state);
    Cache::invalidateTags(['my_settings']);
  }

}
